# encoding: ASCII-8BIT

Encoding.default_external = Encoding::ASCII_8BIT

require "yaml"
require "aws-sdk"
require "find"
require "tempfile"
require "open3"
require "time"
require "optparse"
require "digest"



options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: englaciate [options] [action]"

  opts.on("-U", "--no-upload", "Don't upload archive") {options[:no_upload] = true}
end.parse!


class Integer
  def to_filesize
    {
      'B'  => 1000,
      'KB' => 1000 * 1000,
      'MB' => 1000 * 1000 * 1000,
      'GB' => 1000 * 1000 * 1000 * 1000,
      'TB' => 1000 * 1000 * 1000 * 1000 * 1000
    }.each_pair { |e, s| return "#{(self.to_f / (s / 1000)).round(2)}#{e}" if self < s }
	end
end

def log(str)
	puts str
	STDOUT.flush
end

def aws_list(aws_config, prefix)
	glacier = Aws::Glacier::Client.new(
		region: aws_config[:region],
		credentials: Aws::Credentials.new(aws_config[:access_key_id], aws_config[:secret_access_key])
	)
	inv_start_time = Time.now
	log "Inventory retrieval initiated #{inv_start_time}."
	# Retrieve inventory
	resp = glacier.initiate_job(
		account_id: "-",
		vault_name: aws_config[:vault],
		job_parameters: {
			format: "JSON",
			type: "inventory-retrieval"
		}
	)
	job_status = nil
	begin
		sleep 60 if job_status
		job_status = glacier.describe_job(
			account_id: "-",
			vault_name: aws_config[:vault],
			job_id: resp.job_id
		)
	end until job_status.completed
	log "Inventory retrieval complete after #{(Time.now - inv_start_time).to_i}s."
	inv_resp = glacier.get_job_output(
		account_id: "-",
		job_id: resp.job_id,
		vault_name: aws_config[:vault]
	)
	inventory = JSON.parse(inv_resp.body.read)
	archive_names = []
	inventory["ArchiveList"].each do |a|
		if a["ArchiveDescription"].start_with? prefix
			log a["ArchiveDescription"]
		end
	end
end

def aws_download(aws_config, prefix)
	glacier = Aws::Glacier::Client.new(
		region: aws_config[:region],
		credentials: Aws::Credentials.new(aws_config[:access_key_id], aws_config[:secret_access_key])
	)
	inv_start_time = Time.now
	log "Inventory retrieval initiated #{inv_start_time}."
	# Retrieve inventory
	resp = glacier.initiate_job(
		account_id: "-",
		vault_name: aws_config[:vault],
		job_parameters: {
			format: "JSON",
			type: "inventory-retrieval"
		}
	)
	job_status = nil
	begin
		sleep 60 if job_status
		job_status = glacier.describe_job(
			account_id: "-",
			vault_name: aws_config[:vault],
			job_id: resp.job_id
		)
	end until job_status.completed
	log "Inventory retrieval complete after #{(Time.now - inv_start_time).to_i}s."
	inv_resp = glacier.get_job_output(
		account_id: "-",
		job_id: resp.job_id,
		vault_name: aws_config[:vault]
	)
	inventory = JSON.parse(inv_resp.body.read)
	archive_names = {}
	inventory["ArchiveList"].each do |a|
		if a["ArchiveDescription"].start_with? prefix
			archive_names[a["ArchiveDescription"]] = {id: a["ArchiveId"]}
		end
	end
	arch_start_time = Time.now
	log "Archive retrieval started #{arch_start_time}"
	archive_names.each do |archive_name, archive_data|
		next if File.exists? archive_name
		if archive_name =~ %r|[/\s"']| || !archive_name.start_with? # just in case
			log "Archive name #{archive_name} is weird."
			exit 1
		end
		log "Retrieving #{archive_name}"
		archive_names[archive_name][:resp] = glacier.initiate_job(
			account_id: "-",
			vault_name: aws_config[:vault],
			job_parameters: {
				format: "JSON",
				type: "archive-retrieval",
				archive_id: archive_data[:id]
			}
		)
	end
	first_job = true
	until archive_names.values.map{|v| v[:done]}.all?
		sleep 60 unless first_job
		print "."
		STDOUT.flush
		archive_names.each do |archive_name, archive_data|
			job_status = glacier.describe_job(
				account_id: "-",
				vault_name: aws_config[:vault],
				job_id: resp.job_id
			)
			next unless job_status.completed
			log "Archive #{archive_name} retrieval complete after #{(Time.now - arch_start_time).to_i}s."
			io = File.open(archive_name, "w")
			arch_resp = glacier.get_job_output(
				response_target: io,
				account_id: "-",
				job_id: resp.job_id,
				vault_name: aws_config[:vault]
			)
			io.close
		end
	end
end

def extract_backup(filename)
	`tar xf "#{filename}"`
end

def aws_upload(aws_config, filename)
	log "Uploading to Glacier..."
	glacier = Aws::Glacier::Client.new(
		region: aws_config[:region],
		credentials: Aws::Credentials.new(aws_config[:access_key_id], aws_config[:secret_access_key])
	)
	file = File.open(filename, "r")
	response = glacier.upload_archive(
		vault_name: aws_config[:vault],
		account_id: "-",
		archive_description: filename,
		body: file
	)
	log "Done."
	response
end

def log(message)
	STDERR.puts message
end

def logn(message)
	STDERR.write message
	STDERR.flush
end

def thaw_backups(config)
	if config[:gpg_key_id]
		passphrase_options = if config[:gpg_passphrase]
			passphrase_file = Tempfile.new("englaciate")
			passphrase_file.write(config[:gpg_passphrase])
			passphrase_file.close
			"--pinentry-mode=loopback --batch --passphrase-file #{passphrase_file.path}"
		else
			""
		end
		gpg_command = "gpg2 #{passphrase_options} -d --verify |"
		gpg_suffix = ".gpg"
	end
	backup_archives = Dir.glob("englaciate_backup_*.tar.#{config[:compressor_suffix]}#{gpg_suffix}")
	archives_metadata = {}
	
	logn "Parsing archives for metadata & calculating hashes"
	
	
	sorted_archives = []
	
	backup_archives.each do |ar|
		logn "."
		archives_metadata[ar] = JSON.parse(`cat #{ar} | #{gpg_command} #{config[:uncompressor_command] ? config[:uncompressor_command]+ " |" : ""} tar xO .englaciated`)
		archives_metadata[ar]["digest"] = Digest::SHA512.file(ar).hexdigest
		if !archives_metadata[ar]["previous_archive_sha512"]
			if !sorted_archives[0]
				sorted_archives << ar
			else
				log ""
				log "Two chain starts found: #{ar} and #{sorted_archives[0]}. Remove the wrong one."
				exit 1
			end
		end
	end
	
	log ""
	
	if sorted_archives.empty?
		log "Chain start not found."
		exit 1
	end
	
	(backup_archives.count - 1).times do
		a = backup_archives.detect {|a| archives_metadata[a]["previous_archive_sha512"] == archives_metadata[sorted_archives.last]["digest"]}
		sorted_archives << a if a
	end
	
	if sorted_archives.count != backup_archives.count
		log "Archives miscount: built chain is #{sorted_archives.count} entries long, #{backup_archives.count} entries total"
		exit 1
	end
	
	actual_filelist = Tempfile.new("englaciate")
	actual_filelist.write(`#{gpg_command} < #{sorted_archives.last} | tar xO .englaciated.list`)
	actual_filelist.close
	
	config_path = Dir.getwd
	Dir.chdir(config[:data_dir])
	
	logn "Extracting archives"
	sorted_archives.each do |ar|
		`cat "#{config_path}/#{ar}" | #{gpg_command} #{config[:uncompressor_command] ? config[:uncompressor_command]+ " |" : ""} tar x -T #{actual_filelist.path}#{if sorted_archives.last != ar then " --exclude .englaciated --exclude .englaciated.list" end}`
		logn "."
	end
	
	log ""
	
	FileUtils.mv ".englaciated", (config_path + "/" + ".englaciated")
	FileUtils.mv ".englaciated.list", (config_path + "/" + ".englaciated.list")
	
	log "All done"
end

def create_backup(config)
	current_data = if File.exists? ".englaciated"
		JSON.load(open(".englaciated"))
	else
		{
			last_timestamp: 0
		}
	end

	last_timestamp = Time.at(current_data["last_timestamp"].to_r / 1e9.to_r)
	if current_data["last_timestamp"] != 0
		log "Latest timestamp in backup: #{last_timestamp}."
	else
		log "Starting first backup."
	end

	prev_file_list = if File.exists? ".englaciated.list"
		File.readlines(".englaciated.list").map{|n| n.chomp}.product([true]).to_h
	else
		{}
	end

	current_file_list = {}

	englaciate_ignore_patterns = [/^.englaciate/, /^englaciate_backup_/]

	config_path = Dir.getwd
	Dir.chdir(config[:data_dir])

	Find.find(".") do |path|
		truepath = path.gsub(%r|^\./|, "") + (File.directory?(path) ? "/" : "")
		if (config[:ignore_patterns].map{|s| Regexp.compile s} + englaciate_ignore_patterns).map {|ip| truepath =~ ip}.any?
			Find.prune
		end
		if path != "."
			current_file_list[truepath] = File.lstat(truepath)
		end
	end

	backup_list = []
	size_so_far = 0

	latest_timestamp = last_timestamp

	current_file_list.sort_by{|_, s| s.mtime}.each do |file_path, file_stat|
		file_in_backup = prev_file_list[file_path]
		file_newer_than_backup = (file_stat.mtime > last_timestamp)
		file_not_directory =  !file_path.end_with?("/")
		if (file_in_backup && file_newer_than_backup && file_not_directory || !file_in_backup) && (config[:maximum_archive_size] == 0 || (config[:maximum_archive_size] - size_so_far - file_stat.size) >= 0)
			backup_list << file_path
			size_so_far += file_stat.size
			latest_timestamp = file_stat.mtime if latest_timestamp < file_stat.mtime
		end
	end

	if size_so_far < config[:backup_threshold] && current_data["last_timestamp"] != 0
		log "Not enough changes (#{size_so_far.to_filesize}) to create a new backup, threshold: #{config[:backup_threshold].to_filesize}"
		exit 1
	end

	log "Creating new backup with #{size_so_far.to_filesize} uncompressed files"

	elist_file = Tempfile.new("englaciate")
	e_file = Tempfile.new("englaciate")

	# exclude old .englaciated*

	backup_list.reject!{|s| [".englaciated", ".englaciated.list"].include? s}
	tarlist = Tempfile.new("englaciate")
	tarlist.write((backup_list + [elist_file.path, e_file.path]).uniq.join("\0"))

	backup_list << ".englaciated" << ".englaciated.list"

	filename = "englaciate_backup_#{config[:backup_prefix]}_#{Time.now.strftime("%y_%m_%d_%H%M_%S")}.tar.#{config[:compressor_suffix]}"

	passphrase_file = nil

	if config[:gpg_key_id]
		passphrase_options = if config[:gpg_passphrase]
			passphrase_file = Tempfile.new("englaciate")
			passphrase_file.write(config[:gpg_passphrase])
			passphrase_file.close
			"--pinentry-mode=loopback --batch --passphrase-file #{passphrase_file.path}"
		else
			""
		end
		gpg_command = "gpg2 #{passphrase_options} -se --compression-algo none -u '#{config[:gpg_key_id]}' -r '#{config[:gpg_key_id]}'"
		filename += ".gpg"
	end

	# Write new .englaciated.list

	elist_file.write((prev_file_list.keys + backup_list).uniq.join("\n"))
	elist_file.close

	# Write new .englaciated

	e_file.write(current_data.merge({"last_timestamp" => (latest_timestamp.to_r * 1e9.to_r).to_i}).to_json)
	e_file.close

	tarlist.close

	log "Archiving files..."

	err_r, err_w = IO.pipe

	pipeline = ["tar c --xform='s|^#{elist_file.path.gsub(%r|^/|,"")}$|.englaciated.list|;s|^#{e_file.path.gsub(%r|^/|,"")}$|.englaciated|' --no-recursion --no-unquote --null -T #{tarlist.path}"]
	pipeline << config[:compressor_command]
	pipeline << "pv -atb" unless config[:nopv]
	pipeline << gpg_command if config[:gpg_passphrase] && config[:gpg_key_id]
	pipeline << "cat > #{config_path + "/" + filename}"

	statuses = Open3.pipeline(*pipeline, :err=>STDERR)
	if !(statuses.all?{|s| s.exitstatus == 0})
		log "Tarring failed."
		exit 1
	end

	log "Calculating hash..."

	# Calculate digest
	digest = Digest::SHA512.file(config_path + "/" + filename).hexdigest

	# Move englaciated special files in current dir

	FileUtils.cp elist_file.path, config_path + "/" + ".englaciated.list"

	e_file_new = File.open(config_path + "/" + ".englaciated", "w")
	e_file_new.write(current_data.merge({"last_timestamp" => (latest_timestamp.to_r * 1e9.to_r).to_i, "previous_archive_sha512" => digest}).to_json)

	# Done, cleanup.

	tarlist.unlink
	elist_file.unlink
	e_file.unlink

	# AWS upload

	if config[:aws] && !options[:no_upload]
		aws_upload(config[:aws], filename)
	end

	log filename
end

default_config = {
	maximum_archive_size: 0,
	ignore_patterns: [],
	backup_threshold: 0,
	compressor_command: "xz",
	uncompressor_command: "xz -d",
	compressor_suffix: "xz",
	data_dir: "."
}

config = if File.exists? ".englaciate"
	YAML.load_file ".englaciate"
else
	{}
end

config = default_config.update(config)
config[:backup_prefix] ||= File.absolute_path(config[:data_dir]).split("/").last


case ARGV[0]
when "upload"
	aws_upload(config[:aws], ARGV[1])
	exit 0
when "download"
	aws_download(config[:aws], config[:backup_prefix])
	exit 0
when "list-archives"
	aws_list(config[:aws], config[:backup_prefix])
	exit 0
when "thaw"
	thaw_backups(config)
else
	create_backup(config)
end

